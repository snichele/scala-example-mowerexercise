
object XebiaMowerTest {
  
  class Tuple2Wrapper[A](t: Tuple2[Seq[A], Seq[A]]) {
    def flatjoin = List(t._1,t._2).flatten
  }
  
  implicit def tupleWithCharSeqFlatJoin[Char](t: Tuple2[Seq[Char], Seq[Char]]) = new Tuple2Wrapper(t)
  
  def solveProblem {
    val commands = "GADAAGADAA"
    val directions = "NSEW"
    val directionsClockwise = Seq('N','E','S','W')
    val nextPositionClockwise = (from: Char) => directionsClockwise.span(!_.equals(from)).swap.flatjoin.tail.head
    val nextPositionCounterClockwise = (from: Char) => directionsClockwise.reverse.span(!_.equals(from)).swap.flatjoin.tail.head
    def transcodeCommandWithRealPosition(command: Char)(implicit fromPosition: Char) = {
      command match {
        case 'G' => nextPositionCounterClockwise(fromPosition)
        case 'D' => nextPositionClockwise(fromPosition)
        case 'A' => 'A'
      }
    }
  }
}
